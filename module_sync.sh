#!/bin/bash

# Define the source and destination directories
SOURCE_DIR=~/repos/allaccessible/aa-drupal-plugin/all_accessible
DEST_DIR=~/repos/allaccessible/drupal-test/web/modules/custom/all_accessible

# Perform an initial sync
if [ ! -d "$DEST_DIR" ]; then
    mkdir -p "$DEST_DIR" || { echo "Failed to create destination directory"; exit 1; }
fi

# Clear the destination directory
rm -rf $DEST_DIR/* || { echo "Failed to clear destination directory"; exit 1; }
echo "Destination directory cleared"

# Copy files from source to destination
cp -r $SOURCE_DIR/* $DEST_DIR/ || { echo "Failed to copy files"; exit 1; }
echo "Initial sync complete: $(date)"

# Monitor the source directory for changes and sync on changes
fswatch -r $SOURCE_DIR |
while read -r event; do
  # Clear the destination directory
  rm -rf $DEST_DIR/* || { echo "Failed to clear destination directory"; exit 1; }
  echo "Destination directory cleared"

  # Copy files from source to destination
  cp -r $SOURCE_DIR/* $DEST_DIR/ || { echo "Failed to copy files"; exit 1; }
  echo "Sync complete: $(date)"
done
