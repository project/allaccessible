<?php

namespace Drupal\all_accessible\Form;

use Drupal;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use GuzzleHttp\Exception\GuzzleException;

/**
 * Configure AllAccessible account settings for this site.
 */
class ActivationForm extends ConfigFormBase
{

    /**
     * {@inheritdoc}
     */
    protected function getEditableConfigNames(): array
    {
        return [
            'all_accessible.settings',
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function getFormId(): string
    {
        return 'all_accessible_activation_form';
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(array $form, FormStateInterface $form_state): array
    {
        $form = parent::buildForm($form, $form_state);
        $config = $this->config('all_accessible.settings');

        $form['aa_account_id'] = [
            '#type' => 'textfield',
            '#default_value' => $config->get('aa_account_id'),
            '#required' => FALSE,
        ];

        $form['aa_email'] = [
            '#type' => 'textfield',
            '#default_value' => $config->get('aa_email'),
            '#required' => TRUE,
        ];

        $form['aa_site_id'] = [
          '#type' => 'textfield',
          '#default_value' => $config->get('aa_site_id'),
          '#required' => FALSE,
        ];

        $form['actions']['submit'] = [
            '#type' => 'submit',
            '#value' => $this->t('Upgrade to Premium'),
            '#button_type' => 'primary',
        ];

        return $form;
    }

    /**
     * {@inheritdoc}
     */
    public function submitForm(array &$form, FormStateInterface $form_state): void
    {
        $site_url = Url::fromUri('base:/', ['absolute' => TRUE])->toString();

        try {
            $add_site_response = Drupal::httpClient()->post(
                'https://app.allaccessible.org/api/add-site',
                [
                    'json' => [
                        'email' => $form_state->getValue('aa_email'),
                        'url' => $site_url,
                        'source' => 'drupal',
                    ]
                ]
            );

            if ($add_site_response->getStatusCode() !== 200) {
                $this->messenger()->addError($this->t("Failed to activate AllAccessible. Server responded with non 200 response."));
                return;
            }

            $account_id = $add_site_response->getBody();

            $site_id_response = Drupal::httpClient()->post(
                'https://app.allaccessible.org/api/get-site-id/',
                [
                    'json' => [
                        'pageUrl' => $site_url,
                        'accountID' => $account_id
                    ]
                ]
            );

            $this->config('all_accessible.settings')
                ->set('aa_account_id', $account_id)
                ->set('aa_email', $form_state->getValue('aa_email'))
                ->set('aa_site_id', $site_id_response->getBody())
                ->save();
            $this->messenger()->addMessage($this->t('Successfully activated your new account.'));

        } catch (GuzzleException $e) {
            $this->messenger()->addError($this->t('Failed to activate AllAccessible. Unknown HTTP error occurred. ' . $e->getMessage()));
            return;
        }
    }
}
