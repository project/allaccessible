<?php

namespace Drupal\all_accessible\Form;

use Drupal;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\State\StateInterface;
use GuzzleHttp\Exception\GuzzleException;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configure AllAccessible widget options for this site.
 */
class SiteOptionsForm extends FormBase
{

    /**
     * @var StateInterface
     */
    protected StateInterface $state;

    /**
     * Constructs a new SiteOptionsForm.
     *
     * @param StateInterface $state
     *   The state service.
     */
    public function __construct(StateInterface $state)
    {
        $this->state = $state;
    }

    /**
     * {@inheritdoc}
     */
    public static function create(ContainerInterface $container): SiteOptionsForm|static
    {
        return new static(
            $container->get('state')
        );
    }

    /**
     * {@inheritdoc}
     */
    public function getFormId(): string
    {
        return 'all_accessible_site_options_form';
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(array $form, FormStateInterface $form_state): array
    {
        $state = $this->state;

        $form['widget_color'] = [
            '#type' => 'color',
            //'#title' => $this->t('Chose Widget Color'),
            '#default_value' => $state->get('aacb-triggerBtnBg'),
            '#attributes' => [
                'class' => ['aacb_form-control'],
                'id' => 'colorPicker',
            ],
        ];

        $form['widget_size'] = [
            '#type' => 'select',
            //'#title' => $this->t('Chose Widget Size'),
            '#default_value' => $state->get('aacb-triggerBtnSize', '55'),
            '#attributes' => [
                'id' => 'sizeSelect',
            ],
            '#options' => [
                '40' => $this->t('Small'),
                '55' => $this->t('Medium (Default)'),
                '80' => $this->t('Large'),
            ],
        ];

        $form['widget_shape'] = [
            '#type' => 'select',
            //'#title' => $this->t('Chose Widget Shape'),
            '#default_value' => $state->get('aacb-triggerBtnRadius', '50'),
            '#attributes' => [
                'id' => 'shapeSelect',
            ],
            '#options' => [
                '50' => $this->t('Circle'),
                '0' => $this->t('Square'),
            ],
        ];

        $form['widget_icon'] = [
            '#type' => 'select',
            //'#title' => $this->t('Chose Widget Icon'),
            '#default_value' => $state->get('aacb-triggerSVG', 'Default'),
            '#attributes' => [
                'id' => 'kt_docs_select2_icon',
            ],
            '#options' => [
                'Default' => $this->t('Default'),
                'Alt' => $this->t('Alt'),
                'Adjust' => $this->t('Adjust'),
                'Chair2' => $this->t('Chair2'),
                'Chair3' => $this->t('Chair3'),
                'Heart' => $this->t('Heart'),
                'Braille' => $this->t('Braille'),
                'Blind' => $this->t('Blind'),
                'Eye' => $this->t('Eye'),
                'Globe' => $this->t('Globe'),
                'Access' => $this->t('Access'),
                'Alt2' => $this->t('Alt2'),
                'Cogs' => $this->t('Cogs'),
                'Cane' => $this->t('Cane'),
            ],
        ];

        $form['widget_position'] = [
            '#type' => 'select',
            //'#title' => $this->t('Chose Widget Position'),
            '#default_value' => $state->get('aacb-buttonPosition', 'bottom-right'),
            '#attributes' => [
                'id' => 'buttonPosition',
            ],
            '#options' => [
                'bottom-right' => $this->t('Bottom-Right'),
                'right-center' => $this->t('Middle-Right'),
                'top-right' => $this->t('Top-Right'),
                'bottom-center' => $this->t('Bottom-Center'),
                'bottom-left' => $this->t('Bottom-Left'),
                'left-center' => $this->t('Middle-Left'),
                'top-left' => $this->t('Top-Left'),
            ],
        ];

        $form['white_label'] = [
            '#type' => 'select',
            //'#title' => $this->t('Hide AllAccessible Branding'),
            '#default_value' => $state->get('aacb-isWhiteLabel', 'false'),
            '#attributes' => [
                'id' => 'isWhitelabel',
            ],
            '#options' => [
                'false' => $this->t('Branding Removed'),
                'true' => $this->t('Branding Visible'),
            ],
        ];

        $form['actions']['#type'] = 'actions';
        $form['actions']['submit'] = [
            '#type' => 'submit',
            '#value' => $this->t('Save'),
            '#button_type' => 'primary'
        ];

        return $form;
    }

    /**
     * {@inheritdoc}
     */
    public function submitForm(array &$form, FormStateInterface $form_state): void
    {
        $site_id = $this->config('all_accessible.settings')->get('aa_site_id');
        $options = [
            'color' => $form_state->getValue('widget_color'),
            'size' => $form_state->getValue('widget_size'),
            'shape' => $form_state->getValue('widget_shape'),
            'icon' => $form_state->getValue('widget_icon'),
            'position' => $form_state->getValue('widget_position'),
            'isWhiteLabel' => $form_state->getValue('white_label'),
        ];

        try {
            $response = Drupal::httpClient()->post(
                "https://app.allaccessible.org/api/save-site-options/" . $site_id,
                [
                    'json' => [
                        'triggerBtnBg' => $this->hexToRgb($options['color']),
                        'triggerBtnSize' => $options['size'],
                        'triggerBtnRadius' => $options['shape'],
                        'triggerSVG' => $options['icon'],
                        'buttonPosition' => $options['position'],
                        'isWhiteLabel' => $options['isWhiteLabel'],
                    ]
                ]
            );

            if ($response->getStatusCode() != 200) {
                $this->messenger()->addError($this->t("Failed to update site options. Server responded with non 200 response."));
                return;
            }

            $this->state->set('aacb-triggerBtnBg', $options['color']);
            $this->state->set('aacb-triggerBtnSize', $options['size']);
            $this->state->set('aacb-triggerBtnRadius', $options['shape']);
            $this->state->set('aacb-triggerSVG', $options['icon']);
            $this->state->set('aacb-buttonPosition', $options['position']);
            $this->state->set('aacb-isWhiteLabel', $options['isWhiteLabel']);
            $this->messenger()->addMessage($this->t('Site options have been updated.'));

        } catch (GuzzleException $e) {
            $this->messenger()->addError($this->t('Failed to update site options. Unknown HTTP error occurred. ' . $e->getMessage()));
            return;
        }
    }

    /**
     * Converts hexadecimal color code to RGB format.
     *
     * @param string $hex
     *   Hexadecimal color code (e.g., "#RRGGBB").
     *
     * @return string
     *   RGB color string in the format "rgb(r, g, b)".
     */
    function hexToRgb(string $hex): string
    {
        $hex = str_replace('#', '', $hex);

        $r = hexdec(substr($hex, 0, 2));
        $g = hexdec(substr($hex, 2, 2));
        $b = hexdec(substr($hex, 4, 2));

        return "rgb($r, $g, $b)";
    }

}
