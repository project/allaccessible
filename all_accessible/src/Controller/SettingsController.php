<?php

namespace Drupal\all_accessible\Controller;

use Drupal;
use Drupal\Core\Controller\ControllerBase;
use GuzzleHttp\Exception\GuzzleException;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Form\FormBuilderInterface;

/**
 * Class Settings Controller.
 *
 * @package Drupal\all_accessible\Controller
 */
class SettingsController extends ControllerBase
{

    /**
     * The form builder.
     *
     * @var FormBuilderInterface
     */
    protected $formBuilder;

    /**
     * SettingsController constructor.
     *
     * @param FormBuilderInterface $form_builder
     *   The form builder.
     */
    public function __construct(FormBuilderInterface $form_builder)
    {
        $this->formBuilder = $form_builder;
    }

    /**
     * {@inheritdoc}
     */
    public static function create(ContainerInterface $container)
    {
        return new static(
            $container->get('form_builder')
        );
    }

    /**
     * Render the settings form.
     *
     * @return array
     *   The render array for the settings form.
     */
    public function settingsPage(): array
    {
        $activation_form = $this->formBuilder->getForm('Drupal\all_accessible\Form\ActivationForm');
        $site_options_form = $this->formBuilder->getForm('Drupal\all_accessible\Form\SiteOptionsForm');

        $config = $this->config('all_accessible.settings');
        $aa_account_id = $config->get('aa_account_id');
        $site_options = $this->fetchAndUpdateSiteOptions();

        $name = 'AllAccessible';
        if (isset($site_options['aacb-isWhiteLabel']) && !$site_options['aacb-isWhiteLabel'] == "true") {
            $name = $this->t('Accessibility');
        }

        $aa_constants = array(
            'AACB_NAME' => $name,
            'AACB_VERSION' => '1.3.3',
            'AACB_SUPPORT' => 'https://support.allaccessible.org/'
        );

        return [
            '#theme' => 'all_accessible_theme',
            '#activation_form' => $activation_form,
            '#site_options_form' => $site_options_form,
            '#aa_account_id' => $aa_account_id,
            '#site_options' => $site_options,
            '#aa_constants' => $aa_constants,
            '#attached' => [
                'library' => [
                    'all_accessible/settings',
                ],
            ],
        ];
    }

    public function fetchAndUpdateSiteOptions() : ?array
    {
        $site_id = $this->config('all_accessible.settings')->get('aa_site_id');
        if (empty($site_id)) {
            return NULL;
        }

        $default_options = [
            'aacb-triggerBtnBg' =>  '#2b446d',
            'aacb-triggerBtnSize' => '55',
            'aacb-triggerBtnRadius' => '50',
            'aacb-triggerSVG' => 'Default',
            'aacb-buttonPosition' => 'bottom-right',
            'aacb-isWhiteLabel' => 'false',
        ];

        try {
            $response = Drupal::httpClient()->post(
              'https://app.allaccessible.org/api/get-site-options/',
                [
                    'json' => [
                        'siteId' => $site_id
                    ]
                ]
            );

            if ($response->getStatusCode() !== 200) {
                $this->messenger()->addError("Error loading AllAccessible site options, defaults loaded instead.");
                return $default_options;
            }

            $prefixed_options = [];
            $site_options = json_decode($response->getBody(), true);
            foreach ($site_options as $key => $value) {
                $prefixed_key = 'aacb-' . $key;

                if ($key === 'triggerBtnBg') {
                    $matches = [];
                    $rbg_array = [43, 68, 109];
                    preg_match_all('/\d+/', $value, $matches);
                    if (count($matches[0]) >= 3) {
                        $rbg_array = [$matches[0][0], $matches[0][1], $matches[0][2]];
                    }

                    $value = sprintf("#%02x%02x%02x", $rbg_array[0], $rbg_array[1], $rbg_array[2]);
                }

                $this->state()->set($prefixed_key, $value);
                $prefixed_options[$prefixed_key] = $value;
            }

            return array_merge($default_options, $prefixed_options);

        } catch (GuzzleException $e) {
            $this->messenger()->addError("Error loading AllAccessible site options, defaults loaded instead.");
            return $default_options;
        }
    }

}
