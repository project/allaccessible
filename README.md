# Accessibility by AllAccessible #

Achieve WCAG/ADA compliance with AllAccessible, a powerful Drupal accessibility module. Easily installed, it offers a customizable widget for content, design, and orientation control. Compatible with the latest Drupal versions and regularly updated.

## Features ##

Boost your website’s accessibility with AllAccessible’s extensive features:

## ♿ Accessibility Widget ♿ ## 

Empower all visitors to personalize their browsing experience on your website, with our dynamic Accessibility Widget that includes:

- Content Control Features
- Design Control Features
- Orientation Control Features

## AllAccessible Premium: Unlock a New Dimension in Accessibility ##

- **Compliance:** Adheres to WCAG, Section 508, ADA, and ACT rules and best practices.
- **Risk Reduction:** Reduces legal risk and enhances brand image.
- **Innovation:** Foster innovation and extend outreach.

Benefit from a user-friendly interface that allows swift plugin configuration to meet your unique needs.

[youtube https://www.youtube.com/watch?v=Y7RxL_iM1Tc]

Experience AllAccessible Premium with a risk-free 7-day trial, no credit card required. Our service is available at just $10/mo. Non-Profit organizations are eligible to apply for a free license here: https://www.allaccessible.org/get-allaccessible-for-free/ 

Unveil enhanced capabilities with AllAccessible Premium:

- ## ♿ Enhanced Accessibility Widget ♿ ##

  - Online Dictionary
  - Preset User Profiles
  - Text to Speech
  - Accessibility Statement
  - Page Link Tree
  - Accessibility Accommodation Requests

- ## 🤖 AI-Powered Accessibility Remediation 🤖 ##

AllAccessible Premium employs advanced AI technology to continuously scan your website for accessibility gaps, dynamically applying non-invasive adjustments in real-time.

 - Our AI does the heavy lifting for you, including:

  - Automatically adding missing image alt text descriptions
  - Validating Page Structure and Role definitions
  - Validating and correcting Link Names
  - Creating Aria Labels for Screen Reader Optimization

## ✅ Accessibility Scans and Reports ✅ ##

Run unlimited accessibility scans and receive a corresponding report. AllAccessible Premium’s Accessibility reports provide comprehensive and categorized details including:

A Summary of the issues and highlighting the code where any issues occurred

The severity of any issues, including best practices

The Mapping of Accessibility Issues to WCAG, Section 508, ADA, and ACT rules

## 📊 Remediation and Administrative Dashboards 📊 ##

Our user-friendly dashboard interfaces allow you to make quick and easy adjustments to Ai generated content, and user requests, no coding required!

The Administrative dashboards give you complete control over your site allowing you to add users, add sites and subdomains, address accommodation requests, and adjust your application settings.

## ℹ️ Knowledge Base ℹ️ ##

Complete product documentation, instructional videos are available to all users.

For product documentation visit our Knowledge Base

Submit bug reports and feature suggestions, or track changes in the
[issue queue](http://drupal.org/project/issues/AllAccessible)

## 🆘 Dedicated Support 🆘 ##

AllAccessible provides dedicated support, to help answer any questions you may have.

## 🤝 Partner Program 🤝 ##

AllAccessible offers a profitable, flexible, and simple partner program to help digital agencies build their own branded solutions powered by AllAccessible. These solutions provide your clients with competitive accessibility solutions while generating new sources of revenue for you!

Partners can leverage AllAccessible’s technology to create differentiated services across a wide range of website accessibility use cases such as:

- White Label and Resell
- Implementation & Configuration
- Accommodation as a Service
- Remediation Services

**Partner Benefits:**

- Free AllAccessible License for your website
- Discounted Licensing Costs
- Listing on our partner directory
- Access to our partner portal with free resources

Apply to be a partner [here](https://www.allaccessible.org/partner-program).

## Installation and Development

### Prerequisites

Before installing or developing the AllAccessible Drupal module, you need a Drupal site. Follow the [Drupal documentation](https://www.drupal.org/docs/official_docs/local-development-guide) to set up a local site.


## Installation Instructions ##

1. Download the AllAccessible module.
2. Extract the module archive.
3. Place the `all_accessible` folder into your Drupal site's `web/modules/custom` or `modules/custom` directory, depending on your site's configuration.
4. Navigate to the **Extend** page in your Drupal admin interface (`/admin/modules`).
5. Enable the **AllAccessible** module.
6. Configure the module settings under `/admin/config/user-interface/all-accessible`.


### Development Instructions

For development purposes, synchronize the module files from a source to a destination directory using the provided `module_sync.sh` script.

```bash
#!/bin/bash

# Define the source and destination directories
SOURCE_DIR=~/repos/allaccessible/aa-drupal-plugin/all_accessible
DEST_DIR=~/repos/allaccessible/drupal-test/web/modules/custom/all_accessible

# Perform an initial sync
if [ ! -d "$DEST_DIR" ]; then
    mkdir -p "$DEST_DIR" || { echo "Failed to create destination directory"; exit 1; }
fi

# Clear the destination directory
rm -rf $DEST_DIR/* || { echo "Failed to clear destination directory"; exit 1; }
echo "Destination directory cleared"

# Copy files from source to destination
cp -r $SOURCE_DIR/* $DEST_DIR/ || { echo "Failed to copy files"; exit 1; }
echo "Initial sync complete: $(date)"

# Monitor the source directory for changes and sync on changes
fswatch -r $SOURCE_DIR |
while read -r event; do
  # Clear the destination directory
  rm -rf $DEST_DIR/* || { echo "Failed to clear destination directory"; exit 1; }
  echo "Destination directory cleared"

  # Copy files from source to destination
  cp -r $SOURCE_DIR/* $DEST_DIR/ || { echo "Failed to copy files"; exit 1; }
  echo "Sync complete: $(date)"
done